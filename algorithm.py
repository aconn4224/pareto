#!/usr/bin/env python

from __future__ import division

import matplotlib
matplotlib.use('Agg') # no locahost error.

import sys,re
import utilities as UTIL
import networkx as nx
from matplotlib import pylab as PP
from collections import defaultdict
import operator
from numpy import arange,mean,std
from itertools import izip
from matplotlib.pyplot import locator_params
from math import sqrt
import random

random.seed(10301949)

PP.rc('font', **{'sans-serif':'Arial', 'family':'sans-serif'})


""" Plant-inspired greedy algorithm to minimize \alpha * (Steiner) + (1-\alpha) * (Satellite). """


def eval_objective(alpha,G,P2Coord,P2Label):
    return alpha*UTIL.compute_length(G,P2Coord) + (1-alpha)*UTIL.compute_droot(G,P2Coord,P2Label)


def init_tree(P2Coord,P2Label,stem=True):
    """ Creates a stem for the tree from the root to the middle leaf. """

    # Initialize the root and the remaining leafs/cots.
    Tree = nx.Graph()
    Remaining = set()
    for u in P2Label:
        if P2Label[u] == "root":
            Tree.add_node(u)

        elif P2Label[u] == "cotyledon" or P2Label[u] == "leaf":
            Remaining.add(u)

    # Add the stem.
    if stem:

        # Find coordinates of the root and middle leaf.
        Xs,Ys,Zs = [],[],[]
        for p in P2Coord:

            if P2Label[p] == "root":
                xu,yu,zu = P2Coord[p]
            else:
                x,y,z = P2Coord[p]

                Xs.append(x)
                Ys.append(y)
                Zs.append(z)

        # Draw straight line from root to middle leaf: looks more like real plant.
        # http://math.stackexchange.com/questions/799783/slope-of-a-line-in-3d-coordinate-system
        Xs, Ys, Zs = izip(*sorted(izip(Xs, Ys, Zs))) # sorted coordinates together, first by X.

        mid_idx = int(len(Xs)/2) # need int now because importing division.
        xv,yv,zv = Xs[mid_idx],Ys[mid_idx],Zs[mid_idx]
        sx,sy,sz = xv-xu,yv-yu,zv-zu

        # Add stem edges.
        steps = arange(0.0,1.1,0.1)
        for i in xrange(len(steps)-1):

            u = "r" if i == 0 else "stem%.1f" %(steps[i])
            v = "stem%.1f" %(steps[i+1])

            assert not Tree.has_edge(u,v)
            assert v not in P2Coord and v not in P2Label

            Tree.add_edge(u,v)
            P2Coord[v] = (xu + steps[i+1]*sx, yu + steps[i+1]*sy, zu + steps[i+1]*sz)
            P2Label[v] = "stem"


    return Tree,Remaining,P2Coord,P2Label


def doit(filename,method,output):

    # 1. Compute steiner length and droot.
    steiner_finish = UTIL.check_steiner_finish(filename+"_stp_out")
    if steiner_finish:
        G,P2Coord,P2Label  = UTIL.read_steiner(filename+"_stp_out",method)
        x1 = UTIL.compute_length(G,P2Coord)
        y1 = UTIL.compute_droot(G,P2Coord,P2Label)

    # 2. Compute plant length and droot.
    G,P2Coordorig,P2Labelorig  = UTIL.read_plant(filename)
    x0 = UTIL.compute_length(G,P2Coordorig)
    y0 = UTIL.compute_droot(G,P2Coordorig,P2Labelorig)

    # 3. Compute satellite length and droot.
    x2 = UTIL.compute_sat_length(P2Coordorig,P2Labelorig)
    y2 = x2

    # 4. Compare with random spanning tree.
    randx,randy = [],[]
    
    P2Coord = P2Coordorig.copy()
    P2Label = P2Labelorig.copy()    
    nodes = []
    for u in P2Label:
        if P2Label[u] == "root" or P2Label[u] == "cotyledon" or P2Label[u] == "leaf":
            nodes.append(u)

    for _ in xrange(1000): # 1000 random solutions.
        Tree = nx.Graph()
        
        # Random spanning tree.
        topick = list(nodes)
        random.shuffle(topick)
        picked = [topick.pop()] # start with some random node.
        while len(topick) > 0:
            toadd = topick.pop() # select an unpicked node.
            Tree.add_edge(random.choice(picked),toadd) # add edge to a random picked node.
            picked.append(toadd) # add the picked node in.

        UTIL.run_checks(Tree)

        randx.append(UTIL.compute_length(Tree,P2Coord))
        randy.append(UTIL.compute_droot(Tree,P2Coord,P2Label))


    # 5. Compare with structural stability = satellite emanating from centroid.
    stabx,staby = [],[]

    P2Coord = P2Coordorig.copy()
    P2Label = P2Labelorig.copy()

    # Find coordinates of the root and middle leaf.
    Xs,Ys,Zs = [],[],[]
    for p in P2Coord:

        if P2Label[p] == "root":
            continue
        elif P2Label[p] in ["cotyledon","leaf"]:
            x,y,z = P2Coord[p]

            Xs.append(x)
            Ys.append(y)
            Zs.append(z)

    # Add a new node for the centroid.
    P2Coord["ctr"] = (mean(Xs),mean(Ys),mean(Zs))
    P2Label["ctr"] = "stem"

    Tree = nx.Graph()
    Tree.add_node("ctr")
    for u in P2Label:
        if P2Label[u] == "root" or P2Label[u] == "cotyledon" or P2Label[u] == "leaf":
            Tree.add_edge("ctr",u)

    UTIL.run_checks(Tree)
    stabx.append(UTIL.compute_length(Tree,P2Coord))
    staby.append(UTIL.compute_droot(Tree,P2Coord,P2Label))



    # 6. Run algorithm for different values of alpha.
    alphax,alphay = [],[]
    min_dist   = 10000 # distance to the closest alpha.
    min_alpha  = -1    # the closest alpha.
    plant_wins = False # does the plant beat the Pareto line?
    for alpha in arange(0,1.0,0.005):

        # Initialize the tree and remaining points.
        Tree,Remaining,P2Coord,P2Label = init_tree(P2Coordorig.copy(),P2Labelorig.copy(),stem=True)

        steiner_counter = 1
        while len(Remaining) > 0:
            best_obj = 10000
            best_uv = -1

            # For every node in remaining, find its best node in the tree.
            iTree = Tree.copy()
            for u in Tree:
                for v in Remaining:

                    assert u != v

                    # Compute objective with the edge.
                    iTree.add_edge(u,v)
                    temp_obj = eval_objective(alpha,iTree,P2Coord,P2Label)

                    # Check improvement.
                    if temp_obj < best_obj:
                        best_obj = temp_obj
                        best_uv  = (u,v)
                    iTree.remove_node(v)

                    assert Tree.order() == iTree.order()
                    assert Tree.size()  == iTree.size()

            u,v = best_uv
            assert u in Tree and v in Remaining
            assert not Tree.has_edge(u,v)
            
            if 1==1:#stem:
                xu,yu,zu = P2Coord[u]
                xv,yv,zv = P2Coord[v]
                sx,sy,sz = xv-xu,yv-yu,zv-zu
                steps = arange(0.0,1.1,0.1)
                for i in xrange(len(steps)-1):
                    addu = u if i == 0 else "temp%i-%.1f" %(steiner_counter,steps[i])
                    addv = v if i == 9 else "temp%i-%.1f" %(steiner_counter,steps[i+1])

                    assert not Tree.has_edge(addu,addv)
                    if i == 0: assert addu in P2Coord and addu in P2Label
                    if i != 9: 
                        assert addv not in P2Coord and addv not in P2Label
                        P2Coord[addv] = (xu + steps[i+1]*sx, yu + steps[i+1]*sy, zu + steps[i+1]*sz)
                        P2Label[addv] = "stem"

                    Tree.add_edge(addu,addv)

                steiner_counter += 1
            else:
                Tree.add_edge(u,v)

            Remaining.remove(v)


        # Clip unecessary part of the stem, starting from the top.
        for step in arange(1.0,0.0,-0.1): # from 1 to 0.1
            node = "stem%.1f" %(step)

            if Tree.degree(node) == 1:
                Tree.remove_node(node)
                del P2Label[node]
                del P2Coord[node]
            else:
                assert Tree.degree(node) > 1
                break # all others below are necessary.

        alpha_length = UTIL.compute_length(Tree,P2Coord)
        alpha_droot  = UTIL.compute_droot(Tree,P2Coord,P2Label)

        # Don't plot Droot if it's greater than Steiner's.
        if steiner_finish and alpha_droot > y1: continue

        alphax.append(alpha_length)
        alphay.append(alpha_droot)

        # If the plant beats any Pareto point in both dims, the dist is 0.
        if x0 < alpha_length and y0 < alpha_droot:
            plant_wins = True

        if output == "Alpha":
            print "%.3f\t%.3f\t%.3f\t%.3f" %(alpha,eval_objective(alpha,Tree,P2Coord,P2Label),alpha_length,alpha_droot)

    # Add steiner endpoints, if finished. Else, assign steiner to alpha=1 solution.
    if steiner_finish:
        alphax.append(x1) # same as steinerx below.
        alphay.append(y1) # same as steinery below.
    else:
        x1 = alphax[-1]
        y1 = alphay[-1]

    
    if output == "Pareto":

        if "cp" in filename:
            match = re.search('.*/(.+)_(.)_D(\d+)\.cp.txt',filename)
        else:
            match = re.search('.*/(.+)_(.)_D(\d+)\.txt',filename)

        condition = match.group(1)
        plant     = match.group(2)
        time      = int(match.group(3))

        if plant_wins:
            min_dist = 0
        else:
            min_dist = compute_dist_point_line(x0,y0,alphax,alphay)

        # plant, condition, time, dist from plant to pareto, length of plant, dist from stability to pareto, avg distance from rand to pareto + pval.
        pval = 0
        diis = []
        for ii in xrange(len(randx)):
            dii = compute_dist_point_line(randx[ii],randy[ii],alphax,alphay)
            if dii < min_dist:
                pval += 1

            diis.append(dii)

        pval = pval / len(randx)
        print "%s\t%s\t%i\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f" %(plant,condition,time,min_dist,x0,compute_dist_point_line(stabx[0],staby[0],alphax,alphay),mean(diis),std(diis),pval) # x0=plant length
        
        # Plot Steiner, Satellite, Plant, and many values for different alphas.
        # commented is to normalize.
        plantx = x0#( (x0-x1) / (x2-x1) )
        planty = y0#( (y0-y2) / (y1-y2) )

        steinerx = x1#( (x1-x1) / (x2-x1) )
        steinery = y1#( (y1-y2) / (y1-y2) )

        satx = x2#( (x2-x1) / (x2-x1) )
        saty = y2#( (y2-y2) / (y1-y2) )

        # Plot each individual plant.
        #PP.scatter(alphax,alphay,c='grey',marker='s',s=60) # dots.

        # Last point should be Steiner.
        PP.plot(alphax,alphay,c='grey',marker='s',linewidth=3,markersize=8,zorder=1) # dots plus interpolation.

        PP.scatter(stabx,staby,c='#6B8E23',marker='v',linewidth=1,s=150,zorder=2)

        PP.scatter(plantx,planty,c='red',marker='x',linewidth=3,s=300,zorder=2)
        PP.scatter(steinerx,steinery,c='black',marker='s',s=150,zorder=3)
        PP.scatter(satx,saty,c='black',marker='s',s=150,zorder=4)
        PP.xlabel("Total Length (mm)",fontsize=24)
        PP.ylabel("Travel Distance (mm)",fontsize=24)
        
        # Set the x and y limits without the rands in there.
        axes = PP.gca()
        xmin,xmax = axes.get_xlim()
        ymin,ymax = axes.get_ylim()
        PP.xlim(xmin,xmax)
        PP.ylim(ymin,ymax)


        PP.scatter(randx,randy,c='#6495ED',marker='+',linewidth=1,s=100,zorder=2)
        
        #Remove borders on top and right:
        ax = PP.gca()
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')


        #Increase label size:
        ax = PP.gca()
        ax.tick_params(axis='both',which='major',labelsize=24)


        PP.savefig(filename+"_opt_2xbaselines.pdf",transparent=True,bbox_inches='tight')
        PP.close()


def compute_dist_point_line(px,py,alphax,alphay):
    """ Computes distance from the plant to the Pareto line. """
    min_dist = 10000
    dist     = 10000
    for i in xrange(len(alphax)-1):
        x1,y1 = alphax[i],alphay[i]
        x2,y2 = alphax[i+1],alphay[i+1]

        dist = UTIL.DistancePointLine(px,py,x1,y1,x2,y2)

        if dist < min_dist:
            min_dist = dist

    return min_dist


def main():

    filename = sys.argv[1]
    method   = sys.argv[2]
    output   = sys.argv[3]

    doit(filename,method,output)


if __name__ == "__main__":
    main()
