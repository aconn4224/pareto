#!/usr/bin/env python

import networkx as nx
import sys,re,math
import random
from numpy import mean


#==============================================================================
#                                  ERROR CHECKING
#==============================================================================
def run_checks(G):
    """ Error checking on G. """

    # Check if there are any isolated nodes.
    if nx.number_connected_components(G) > 1: assert False

    # Check if truly a tree.
    if G.order() != G.size() + 1: assert False


#==============================================================================
#                                   READ PLANT
#==============================================================================
def extract_label(point):
    """ Returns the label of the given point. """
    if point.startswith("r"):
        return "root"
    elif point.startswith("s"):
        return "stem"
    elif point.startswith("c"):
        return "cotyledon"
    elif point.startswith("b"):
        return "branch"
    elif point.startswith("l"):
        return "leaf"
    else:
        assert False


def read_plant(filename):
    """ Read Adam's input file. """

    G = nx.Graph()
    P2Label = {} # points to label
    P2Coord = {} # points to (x,y,z)
    in_edges = False
    with open(filename) as f:
        for line in f:
            if line.strip() == "": continue # blank lines.
            if line.startswith("#end"): continue
            if line.startswith("#edges"):
                in_edges = True
                continue

            if not in_edges: # parse coordinates.

                # Get rid of quotes, if there.
                line = line.replace("\"","")

                # Split by "," to get the coordinates.
                cols = line.strip().split(",")
                x,y,z = map(float,cols[1:])

                if len(cols) != 4: assert False

                # Extract point and label.
                point = cols[0].split(" ")[0] # "r -nom-"" -> "r"
                label = extract_label(point)  # "r" -> "root"

                P2Coord[point] = (x,y,z)
                P2Label[point] = label
                G.add_node(point)

            else: # parse edges.

                u,neighbors = line.split(":") #r: s1, s2

                for v in neighbors.split(","):
                    v = v.strip()

                    if u not in G or v not in G: assert False

                    G.add_edge(u,v)

    # Check if there are any obvious errors in G.
    run_checks(G)

    return G,P2Coord,P2Label


#==============================================================================
#                              READ STEINER TREES
#==============================================================================
def read_steiner(filename,method):
    """ Driver. """
    if method == "SmithStar":
        return read_steiner_smithstar(filename)
    elif method == "Branch":
        return read_steiner_branch(filename)
    assert False


def read_steiner_smithstar(filename):
    """ Assumes SmithStar algorithm output format. """
    G = nx.Graph()
    P2Label = {} # points to label
    P2Coord = {} # points to (x,y,z)
    first_node = True
    with open(filename) as f:
        for line in f:

            # Input points.
            # > terminal[0] = Point(90.121, 64.664, 403.97)
            if line.startswith("> terminal"):

                match = re.search('(.+)terminal\[(\d+)\](.+)',line)
                if match:
                    u = int(match.group(2))
                    G.add_node(u)

                    point = line[line.index("Point")+6:-2]
                    x,y,z = map(float,point.strip().split(","))
                    P2Coord[u] = (x,y,z)

                    if first_node:
                        P2Label[u] = "root"
                        first_node = False
                    else:
                        P2Label[u] = "leaf"

            # Graph edges.
            # > steiner[14] adjacencies:  22 24 23
            elif "adjacencies" in line:
                match = re.search('(.+)steiner\[(\d+)\](.+)',line)
                if match:
                    u = int(match.group(2))
                    neighbors = map(int,line.strip().split(":")[1].strip().split())
                    for v in neighbors:

                        assert u in G and v in G

                        G.add_edge(u,v)

            # Steiner points.
            # > steiner[14]: 83.40931 119.71333 396.92941
            elif "steiner" in line:
                match = re.search('(.+)steiner\[(\d+)\](.+)',line)
                if match:
                    u = int(match.group(2))
                    G.add_node(u)

                    x,y,z = map(float,line.strip().split(":")[1].strip().split())
                    P2Coord[u] = (x,y,z)
                    P2Label[u] = "branch"

    # Check if there are any obvious errors in G.
    run_checks(G)

    return G,P2Coord,P2Label


def read_steiner_branch(filename):
    """ Parses Branch algorithm output format. """

    G = nx.Graph()
    P2Label = {} # points to label
    P2Coord = {} # points to (x,y,z)
    first_node = True
    in_edges = False
    with open(filename) as f:
        for line in f:

            # Input points.
            # > terminal[0] = Point(90.121, 64.664, 403.97)
            if line.startswith("> terminal"):
                match = re.search('(.+)terminal\[(\d+)\](.+)',line)
                if match:
                    u = int(match.group(2))
                    G.add_node(u)

                    point = line[line.index("Point")+6:-2]
                    x,y,z = map(float,point.strip().split(","))
                    P2Coord[u] = (x,y,z)

                    if first_node:
                        P2Label[u] = "root"
                        first_node = False
                    else:
                        P2Label[u] = "leaf"

            # Graph edges.
            # 10 13
            elif line.strip() == "Edges:":
                in_edges = True
                continue
            elif line.startswith("Evaluated_Level:"):
                in_edges = False
                continue

            # Steiner points.
            # point[13] = 76.773956 119.42951 384.70989
            elif line.startswith("point["):
                match = re.search('point\[(\d+)\](.+)',line)
                if match:
                    u = int(match.group(1))
                    G.add_node(u)

                    x,y,z = map(float,line.strip().split("=")[1].strip().split())
                    P2Coord[u] = (x,y,z)
                    P2Label[u] = "branch"

            if in_edges:
                u,v = map(int,line.strip().split())
                assert u in G and v in G
                G.add_edge(u,v)

    # Check if there are any obvious errors in G.
    run_checks(G)

    return G,P2Coord,P2Label


def check_steiner_finish(filename):
    """ Checks if Steiner finished running. """
    with open(filename) as f:
        for line in f:
            if "Took" in line and "seconds" in line:
                return True

    return False

#==============================================================================
#                             COMPUTE LENGTHS
#==============================================================================
def euclidean_dist(p1,p2,P2Coord):
    """ Computes the Euclidean distance between the two points. """
    x1,y1,z1 = P2Coord[p1]
    x2,y2,z2 = P2Coord[p2]

    return math.sqrt( (x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2)


def compute_length(G,P2Coord):
    """ Computes the total length of the plant. """
    length = sum([euclidean_dist(u,v,P2Coord) for u,v in G.edges_iter()])

    return length

def compute_droot(G,P2Coord,P2Label):
    """ Computes the total distance from each leaf to the root. """

    # Find the root.
    for u in G:
        if P2Label[u] == "root":
            root = u
            break

    # Compute distance from each leaf to the root.
    droot = 0
    for u in G:
        if P2Label[u] == "leaf" or P2Label[u] == "cotyledon":
            path = nx.shortest_path(G,root,u)
            for i in xrange(len(path)-1):
                droot += euclidean_dist(path[i],path[i+1],P2Coord)

    return droot


#==============================================================================
#                             SATELLITE ALGORITHM
#==============================================================================
def generate_satellite(filename):
    """ Generate satellite solution from Adam's input file."""

    G = nx.Graph()
    P2Label = {} # points to label
    P2Coord = {} # points to (x,y,z)
    in_edges = False
    with open(filename) as f:
        for line in f:
            if line.strip() == "": continue # blank lines.
            if line.startswith("#end"): continue
            if line.startswith("#edges"):
                in_edges = True
                continue

            if not in_edges: # parse coordinates.

                # Get rid of quotes, if there.
                line = line.replace("\"","")

                # Split by "," to get the coordinates.
                cols = line.strip().split(",")
                x,y,z = map(float,cols[1:])

                if len(cols) != 4: assert False

                # Extract point and label.
                point = cols[0].split(" ")[0] # "r -nom-"" -> "r"
                label = extract_label(point)  # "r" -> "root"

                if label == "stem" or label == "branch": continue
                assert label == "root" or label == "leaf" or label == "cotyledon"

                P2Coord[point] = (x,y,z)
                P2Label[point] = label

            else: # don't need to parse edges.
                continue

    # Add edges from root to each leaf.
    root = -1
    for u in P2Label:
        if P2Label[u] == "root":
            root = u

        G.add_node(u)

    for u in G:
        if root == u: continue

        assert P2Label[u] == "leaf" or P2Label[u] == "cotyledon"

        G.add_edge(root,u)

    # Check if there are any obvious errors in G.
    run_checks(G)

    return G,P2Coord,P2Label



def compute_sat_length(P2Coord,P2Label):
    """ The total length and total droot for the satellite solution is the same. """

    # Find the root.
    for u in P2Coord:
        if P2Label[u] == "root":
            root = u
            break

    # Compute the distance from each leaf to the root.
    length = 0
    for u in P2Coord:
        if P2Label[u] == "leaf" or P2Label[u] == "cotyledon":
            length += euclidean_dist(root,u,P2Coord)

    return length


#==============================================================================
#                               MST ALGORITHM
#==============================================================================
def compute_mst_length(P2Coord,P2Label):
    G = nx.Graph()
    for u in P2Coord:
        if P2Label[u] not in ["root","cotyledon","leaf"]: continue
        for v in P2Coord:
            if P2Label[v] not in ["root","cotyledon","leaf"]: continue
            if u >= v: continue
            G.add_edge(u,v,weight=euclidean_dist(u,v,P2Coord))

    Gmst = nx.minimum_spanning_tree(G)

    return compute_length(Gmst,P2Coord)


def compute_mst_droot(P2Coord,P2Label):
    G = nx.Graph()
    for u in P2Coord:
        if P2Label[u] not in ["root","cotyledon","leaf"]: continue
        for v in P2Coord:
            if P2Label[v] not in ["root","cotyledon","leaf"]: continue
            if u >= v: continue
            G.add_edge(u,v,weight=euclidean_dist(u,v,P2Coord))

    Gmst = nx.minimum_spanning_tree(G)

    return compute_droot(Gmst,P2Coord,P2Label)


#==============================================================================
#                                RANDOM ALGORITHM
#==============================================================================
def generate_random(filename):
    """ Generate random solution from Adam's input file."""

    G = nx.Graph()
    P2Label = {} # points to label
    P2Coord = {} # points to (x,y,z)
    in_edges = False
    with open(filename) as f:
        for line in f:
            if line.strip() == "": continue # blank lines.
            if line.startswith("#end"): continue
            if line.startswith("#edges"):
                in_edges = True
                continue

            if not in_edges: # parse coordinates.

                # Get rid of quotes, if there.
                line = line.replace("\"","")

                # Split by "," to get the coordinates.
                cols = line.strip().split(",")
                x,y,z = map(float,cols[1:])

                if len(cols) != 4: assert False

                # Extract point and label.
                point = cols[0].split(" ")[0] # "r -nom-"" -> "r"
                label = extract_label(point)  # "r" -> "root"

                if label == "stem" or label == "branch": continue
                assert label == "root" or label == "leaf" or label == "cotyledon"

                P2Coord[point] = (x,y,z)
                P2Label[point] = label

            else: # don't need to parse edges.
                continue


    # Generate random tree.
    nodes = []
    for u in P2Label:
        if P2Label[u] == "root" or P2Label[u] == "cotyledon" or P2Label[u] == "leaf":
            nodes.append(u)

    # new way: random spanning tree.
    topick = list(nodes)
    random.shuffle(topick)
    picked = [topick.pop()] # start with some random node.
    while len(topick) > 0:
        toadd = topick.pop() # select an unpicked node.
        G.add_edge(random.choice(picked),toadd) # add edge to a random picked node.
        picked.append(toadd) # add the picked node in.

    # Check if there are any obvious errors in G.
    run_checks(G)

    return G,P2Coord,P2Label


#==============================================================================
#                                STABLE ALGORITHM
#==============================================================================
def generate_stable(filename):
    """ Generate stable solution from Adam's input file."""

    G = nx.Graph()
    P2Label = {} # points to label
    P2Coord = {} # points to (x,y,z)
    in_edges = False
    with open(filename) as f:
        for line in f:
            if line.strip() == "": continue # blank lines.
            if line.startswith("#end"): continue
            if line.startswith("#edges"):
                in_edges = True
                continue

            if not in_edges: # parse coordinates.

                # Get rid of quotes, if there.
                line = line.replace("\"","")

                # Split by "," to get the coordinates.
                cols = line.strip().split(",")
                x,y,z = map(float,cols[1:])

                if len(cols) != 4: assert False

                # Extract point and label.
                point = cols[0].split(" ")[0] # "r -nom-"" -> "r"
                label = extract_label(point)  # "r" -> "root"

                if label == "stem" or label == "branch": continue
                assert label == "root" or label == "leaf" or label == "cotyledon"

                P2Coord[point] = (x,y,z)
                P2Label[point] = label

            else: # don't need to parse edges.
                continue


    # Generate structual stability tree.

    # Find coordinates of the root and middle leaf.
    Xs,Ys,Zs = [],[],[]
    for p in P2Coord:

        if P2Label[p] == "root":
            continue
        #else:
        elif P2Label[p] in ["cotyledon","leaf"]:
            x,y,z = P2Coord[p]

            Xs.append(x)
            Ys.append(y)
            Zs.append(z)

    # Add a new node for the centroid.
    P2Coord["ctr"] = (mean(Xs),mean(Ys),mean(Zs))
    P2Label["ctr"] = "stem"

    G.add_node("ctr")
    for u in P2Label:
        if P2Label[u] == "root" or P2Label[u] == "cotyledon" or P2Label[u] == "leaf":
            G.add_edge("ctr",u)

    # Check if there are any obvious errors in G.
    run_checks(G)

    return G,P2Coord,P2Label




#==============================================================================
#                     DISTANCE FROM POINT TO A LINE SEGMENT
#==============================================================================
# From: https://nodedangles.wordpress.com/2010/05/16/measuring-distance-from-a-point-to-a-line-segment/

def lineMagnitude (x1, y1, x2, y2):
    lineMagnitude = math.sqrt(math.pow((x2 - x1), 2)+ math.pow((y2 - y1), 2))
    return lineMagnitude
 
#Calc minimum distance from a point and a line segment (i.e. consecutive vertices in a polyline).
def DistancePointLine (px, py, x1, y1, x2, y2):
    #http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
    LineMag = lineMagnitude(x1, y1, x2, y2)
 
    if LineMag < 0.00000001:
        DistancePointLine = 9999
        return DistancePointLine
 
    u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
    u = u1 / (LineMag * LineMag)
 
    if (u < 0.00001) or (u > 1):
        #// closest point does not fall within the line segment, take the shorter distance
        #// to an endpoint
        ix = lineMagnitude(px, py, x1, y1)
        iy = lineMagnitude(px, py, x2, y2)
        if ix > iy:
            DistancePointLine = iy
        else:
            DistancePointLine = ix
    else:
        # Intersecting point is on the line, use the formula
        ix = x1 + u * (x2 - x1)
        iy = y1 + u * (y2 - y1)
        DistancePointLine = lineMagnitude(px, py, ix, iy)
 
    return DistancePointLine