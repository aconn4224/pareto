#!/bin/sh
# Usage: bash run_single.sh
# Runs the whole pipeline for a single plant specified by 'FILENAME'.
# Replicates Figure 3B (Tomato-Ambient).

FILENAME="data/control_B_D20.cp.txt"

echo "generating stp..."
#./generate_stp.py $FILENAME > ${FILENAME}_stp


echo "running MPC15..."
#../MPC15-master/code/SteinerExact/SteinerExact -method SmithStar -input ${FILENAME}_stp > ${FILENAME}_stp_out


echo "running pareto..."
./algorithm.py $FILENAME SmithStar Pareto