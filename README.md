Plant-inspired greedy algorithm to test whether plant architectures are Pareto optimal in two simple objectives: minimizing total length and minimizing travel distance.

To compute the Steiner optimal for the plant architecture, you need to install:
    Steiner Tree exact solver: https://github.com/DIKU-Steiner/MPC15


- generate_stp.py converts the input plant file to a format readable by the Steiner tree exact solver.
- utilities.py contain utilities/helpers.
- algorithm.py contains the plant-inspired greedy algorithm code.

- run_single.sh is a script to run everything together. Run it, and watch the magic!


Contact: Saket Navlakha (navlakha@salk.edu)
