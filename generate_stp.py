#!/usr/bin/env python

import networkx as nx
import sys
import utilities as UTIL

""" Converts the plant format to the stp format to run Steiner analysis. """

def doit(filename):

    # Read graph, coordinates, and labels.
    G,P2Coord,P2Label = UTIL.read_plant(filename)

    # Print stp file for only the root, cotyledons, and leaves.
    for u in G:
        if P2Label[u] == "root":
            root = u
            break
            
    leaves = []
    for u in G:
        if P2Label[u] in ["cotyledon","leaf"]:
            leaves.append(u)
    
    # Write in STP format.
    print "SECTION Graph"
    print "Nodes %i" %(len(leaves)+1) # leaves + 1 root.
    print "END\n"

    print "SECTION Coordinates"

    # First node is the root.
    x,y,z = P2Coord[root]
    print "DDD %i %.3f %.3f %.3f" %(0,x,y,z)

    # Then come the leaves.
    for i,u in enumerate(leaves):
        x,y,z = P2Coord[u]
        print "DDD %i %.3f %.3f %.3f" %(i+1,x,y,z)
        
    print "END"


def main():

    doit(sys.argv[1])



if __name__ == "__main__":
    main()
